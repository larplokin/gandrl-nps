import json
import random
from typing import Generator

with open("data/1m_6ex_karel/train.json", "r") as file:
    DATASET_SIZE = sum(1 for _ in file)


def get_part_file(percent: int) -> None:
    lines_required = set(
        random.sample(range(DATASET_SIZE), percent * DATASET_SIZE // 100)
    )

    with open("data/1m_6ex_karel/train.json", "r") as source, open(
        f"data/1m_6ex_karel/train_{percent}p.json", "w"
    ) as destination:
        line_number = 0
        for line in source:
            if line_number in lines_required:
                destination.write(line)
            line_number += 1


def file_to_iterator(path: str) -> Generator[dict, None, None]:
    with open(path, "r") as file:
        for line in file:
            yield json.loads(line.strip())


if __name__ == "__main__":
    get_part_file(1)
    get_part_file(3)
    get_part_file(10)
